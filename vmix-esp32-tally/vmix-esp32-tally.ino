#include <WiFi.h>
#include <WiFiClient.h>
#include <Preferences.h>
#include <WebServer.h>
#include <stdio.h>
#include <string>  

Preferences preferences;

const char* ssid =  "vMix-ESP32-Tally";
const char* password = "";

const byte led_live = 21;  // red
const byte led_pre  = 22;  // yellow
const byte led_safe = 23;  // green

String header;

String WIFI_SSID;
String WIFI_PASS;
String VMIX_IP = "192.168.1.69";
int VMIX_PORT = 8099;
int TALLY_NR = 1;
int new_tally;
// END CONFIGURATION

WiFiClient client;
WebServer server(80);  // Object of WebServer(HTTP port, 80 is default)

char currentState = -1;

void setup()
{
  Serial.begin(115200);

  setCpuFrequencyMhz(80);
  btStop();
 
  Serial.println("vMix ESP32");
  Serial.println("Wireless Tally");
  Serial.println("by Peter Tellep");
  Serial.println("based on M5Stick-C");
  Serial.println(" Tally by Guido Visser");
  Serial.println();

  pinMode(led_live, OUTPUT);
  pinMode(led_pre, OUTPUT);
  pinMode(led_safe, OUTPUT);
  
  preferences.begin("vMixTally", false);

  Serial.println("SSID from preferences");
  Serial.println(preferences.getString("wifi_ssid").length());

  if(preferences.getString("wifi_ssid").length() > 0) {
    WIFI_SSID = preferences.getString("wifi_ssid");
    WIFI_PASS = preferences.getString("wifi_pass");
    startWiFi();
  } else {
    startLocalWiFi();
  }
 
  TALLY_NR = preferences.getUInt("tally");
  VMIX_IP = preferences.getString("vmix_ip");
    
  preferences.end();

}

void loadWiFiPreferences(){
  Serial.println("load preferences");
  String wifi_ssid;
  String wifi_pass;
  
  preferences.begin("vMixTally", false);
  
  wifi_ssid = preferences.getString("wifi_ssid");
  wifi_pass = preferences.getString("wifi_pass");
  preferences.end();
}

void loadvMixPreferences(){
  int tally_nr;
  String vmix_ip;
  
  preferences.begin("vMixTally", false);

  tally_nr = preferences.getUInt("tally");
  vmix_ip = preferences.getString("vmix_ip");
  preferences.end();  
}

void saveWiFiPreferences(String wifi_ssid, String wifi_pass){
  preferences.begin("vMixTally", false);
  if(wifi_ssid != ""){
    preferences.putString("wifi_ssid", &(WIFI_SSID[0]));
    preferences.putString("wifi_pass", &(WIFI_PASS[0]));
  }
  preferences.end();
}
void savevMixPreferences(){
//  preferences.begin("vMixTally", false);
//  if(tally != "") {
//    TALLY_NR =  std::atoi(tally.c_str());  
//    preferences.putUInt("tally", TALLY_NR);
//  }
//  if(server.arg("ssid") != ""){
//    WIFI_SSID = server.arg("ssid");  
//    WIFI_PASS = server.arg("pwd");
//    preferences.putString("wifi_ssid", &(WIFI_SSID[0]));
//    preferences.putString("wifi_pass", &(WIFI_PASS[0]));
//  }
//  if(server.arg("vmixip") != ""){
//    VMIX_IP = server.arg("vmixip");  
//    preferences.putString("vmix_ip", &(VMIX_IP[0]));
//  }
//  preferences.end();
}

void startServer(){
    server.on("/", handle_root);
    server.on("/save", handle_save);
    server.on("/vmix_settings", handle_vmix_settings);
    server.on("/save_vmix_settings", handle_save_vmix_settings);  
    server.begin();
    Serial.println("HTTP server started");
    Serial.println("Connect to WiFi");
}
void startWiFi(){
    WiFi.mode(WIFI_STA);
    WiFi.begin(&(WIFI_SSID[0]), &(WIFI_PASS[0]));
    
    // We start by connecting to a WiFi network
    
    Serial.println("Waiting for WiFi...");
  
    int tries = 0;
    boolean wifi_connected = true;
    
    while(WiFi.waitForConnectResult() != WL_CONNECTED){
      Serial.print(".");
      delay(1000);
      tries++;
      if(tries > 10){
        tries = 0;
        Serial.println("Wifi connection failed, start local wifi");
        wifi_connected = false;
        startLocalWiFi();
        break;        
      }
    } 
    
    if(wifi_connected == false){
      startLocalWiFi();
    } else {
      
      Serial.println("WiFi connected");
      Serial.println("IP address: ");
      Serial.print(WiFi.localIP());
      Serial.println(WiFi.localIP());
      startServer();
      connectTovMix();     
    }
}

// This starts the M5Stack as a WiFi Access Point so you can configure it 
void startLocalWiFi() {
  WiFi.mode(WIFI_AP);
  WiFi.softAP("vMix-M5Stack-Tally", "12345678");
  startServer();
}


// Connect to vMix instance
boolean connectTovMix()
{
  Serial.println("Connecting to vMix...");

  if (client.connect(&(VMIX_IP[0]),VMIX_PORT))
  {
    Serial.println("Connected to vMix!");
    Serial.println("------------");

    // Subscribe to the tally events
    client.println("SUBSCRIBE TALLY");
    showTallyScreen();
    return true;
  }
  else
  {
    char tryCount = 0;
    Serial.println("Could not connect to vMix");
    Serial.println("Retrying: 0/3");
    boolean retry = false;
    for (int i = 0; i < 3; i++)
    {
      Serial.print(i);
      retry = retryConnectionvMix(i);
      if (!retry) {
        return true;
      }
    }
    Serial.println("Couldn't connect to vMix");
    Serial.println();
    Serial.println("Please restart device");
    Serial.println("to retry");
    return false;
  }
}

boolean retryConnectionvMix(char tryCount) {
 
  Serial.println("Couldn't connect to vMix");
  Serial.print("Retrying: ");
  Serial.print(tryCount);
  Serial.print("/3");
  delay(2000);
  boolean conn = connectTovMix();
  if (conn) {
    return false;
  }
  return true;
}

// Handle Tally State
void displayTallyState(uint16_t bgcolor, uint16_t color, int x, int y, String state){
  Serial.println(state);
}  

// Handle incoming data
void handleData(String data)
{
  // Check if server data is tally data
  if (data.indexOf("TALLY") == 0)
  {
    char newState = data.charAt(TALLY_NR + 9);
    // Check if tally state has changed
    if (currentState != newState)
    {
      currentState = newState;
      showTallyScreen();
    }
  }
  else
  {
    Serial.print("Response from vMix: ");
    Serial.println(data);
  }
}

void showTallyScreen() {

  switch (currentState)
  {
    case '0':
      // SAFE
      digitalWrite(led_safe, HIGH);
      digitalWrite(led_pre, LOW);
      digitalWrite(led_live, LOW);
      break;
    case '1':
      // LIVE
      digitalWrite(led_safe, LOW);
      digitalWrite(led_pre, LOW);
      digitalWrite(led_live, HIGH);
      break;
    case '2':
      // PRE
      digitalWrite(led_safe, LOW);
      digitalWrite(led_pre, HIGH);
      digitalWrite(led_live, LOW);
      break;
    default:
      // SAFE
      digitalWrite(led_safe, HIGH);
      digitalWrite(led_pre, LOW);
      digitalWrite(led_live, LOW);
  }
  String camera = "CAMERA: " + (String)(TALLY_NR +1);
  Serial.println(camera);
}

void showMsg(const char* msg){
  Serial.println(msg);
}

void showTallyNum(String msg){
  Serial.println(msg);
}

void showAPScreen() {
  Serial.println("Showing Access Point Screen");
  Serial.println("Unable to connect to WiFi");
  Serial.println();
  Serial.println("Please connect to:");
  Serial.println();
  Serial.println("SSID: vMix-M5tack-Tally");
  Serial.println("Pwd: 12345678");
  Serial.println();
  Serial.println("Open http://192.168.4.1 \n in a browser and \n configure your WiFi");
  Serial.println();
}

void showTallySetScreen() {
  Serial.println("Showing Tally Set Screen");
  Serial.print("Current Camera: ");
  Serial.print(TALLY_NR + 1);
  Serial.print("New Camera: ");
  Serial.print(new_tally + 1);
  Serial.print("SAVE");
  Serial.print("-");
  Serial.print("+");
}


// WEBSERVER STUFF
String HEADER =  "<!DOCTYPE html>\
  <html lang='en'>\
  <head>\
  <meta charset='UTF-8'>\
  <meta name='viewport' content='width=device-width, initial-scale=1.0, shrink-to-fit=no'>\
  <title>vMix M5Stick-C Tally</title>\
  <link rel='stylesheet' type='text/css' href='style.css'>\
  <style>\
  .wrapper,input{width:100%}body,html{padding:0;margin:0}.wrapper{padding:10px;box-sizing:border-box}.wrapper h1{text-align:center}input[type=submit]{width:50px;margin:10px auto}\
  </style>\
  </head>\
  <body>\
  <div class='wrapper'>";

String FOOTER = "</div>\
  </body>\
  </html>";
  
void handle_root() {
  String tally = (String)TALLY_NR;
  String HTML = HEADER;
  HTML += "<div class='wrapper'>";
  HTML += "<h1>vMix M5Stack Tally Settings</h1>";
  HTML += "<form action='/save' method='post'>";
  HTML += "SSID:<br/>";
  HTML += "<input type='text' name='ssid' value='" + (String)WIFI_SSID + "'>";
  HTML += "Password:<br/>";
  HTML += "<input type='text' name='pwd' value='" + (String)WIFI_PASS + "'>";
  HTML += "vMix IP Address:<br/>";
  HTML += "<input type='text' name='vmixip' value ='" + (String)VMIX_IP + "'>";
  HTML += "Tally Number:<br/>";
  HTML += "<input type='number' name='tally_num' value='" + tally + "'>";
  HTML += "<input type='submit' value='SAVE' class='btn btn-primary'>";
  HTML += "</form>";
  HTML += FOOTER;
  
  server.send(200, "text/html", HTML);
}

void handle_vmix_settings() {
  String tally = (String)TALLY_NR;
  String HTML = HEADER;
  HTML += "<div class='wrapper'>";
  HTML += "<h1>vMix M5Stack Tally Settings</h1>";
  HTML += "<form action='/save_vmix_settings' method='post'>";
  HTML += "vMix IP Address:<br/>";
  HTML += "<input type='text' name='vmixip' value ='" + (String)VMIX_IP + "'>";
  HTML += "Tally Number:<br/>";
  HTML += "<input type='number' name='tally_num' value='" + tally + "'>";
  HTML += "<input type='submit' value='SAVE' class='btn btn-primary'>";
  HTML += "</form>";
  HTML += FOOTER;
  
  server.send(200, "text/html", HTML);
  
}

void handle_save() {
  String message = "";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  message += " direct:" + server.arg("tally_num");
  server.send(404, "text/plain", message);
  Serial.println(message);

  
  showTallyNum(server.arg("tally_num"));
  String tally = server.arg("tally_num");

  // save value in preferences
  preferences.begin("vMixTally", false);
  if(tally != "") {
    TALLY_NR =  std::atoi(tally.c_str());  
    preferences.putUInt("tally", TALLY_NR);
  }
  if(server.arg("ssid") != ""){
    WIFI_SSID = server.arg("ssid");  
    WIFI_PASS = server.arg("pwd");
    preferences.putString("wifi_ssid", &(WIFI_SSID[0]));
    preferences.putString("wifi_pass", &(WIFI_PASS[0]));
  }
  if(server.arg("vmixip") != ""){
    VMIX_IP = server.arg("vmixip");  
    preferences.putString("vmix_ip", &(VMIX_IP[0]));
  }
  preferences.end();
  
  // We start by connecting to a WiFi network
  Serial.println(WIFI_SSID);
  Serial.println(WIFI_PASS);
  delay(2000);
  
  Serial.println();
  Serial.println("Waiting for WiFi...");

  int tries = 0;
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(1000);
    tries++;
    if(tries > 10){
      tries = 0;
      break;
    }
  }
  
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.print(WiFi.localIP());


  delay(2000);
  connectTovMix();
}

void handle_save_vmix_settings() {
  String message = "";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  message += " direct:" + server.arg("tally_num");
  server.send(404, "text/plain", message);
  Serial.println(message);

  showTallyNum(server.arg("tally_num"));
  String tally = server.arg("tally_num");

  // save value in preferences
  preferences.begin("vMixTally", false);
  if(tally != "") {
    TALLY_NR =  std::atoi(tally.c_str());  
    preferences.putUInt("tally", TALLY_NR);
  }
  if(server.arg("vmixip") != ""){
    VMIX_IP = server.arg("vmixip");  
    preferences.putString("vmix_ip", &(VMIX_IP[0]));
  }
  preferences.end();
  //startWiFi();  
  Serial.println("Restarting");
  ESP.restart();
}

void updateTallyNR(int tally){
  preferences.begin("vMixTally", false);
  if(tally >= 0) {
    TALLY_NR =  tally;  
    preferences.putUInt("tally", TALLY_NR);
  }
  preferences.end();
  connectTovMix(); 
}
void loop()
{
  
  server.handleClient();

  while (client.available())
  {
    String data = client.readStringUntil('\r\n');
    handleData(data);
  }

}
